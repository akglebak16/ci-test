const path = require("path");
const gmail = require("gmail-tester");

async function getUrl(email){
  const emailSubject = !process.env.TO_EMAIL ? "Welcome to Prime Opinion": `primeoplogin+${email}gmail.com Welcome to Prime Opinion`
  const toEmail = !process.env.TO_EMAIL ? 'primeoplogin+' + email +"gmail.com": "primeinsightsdev@gmail.com"
  return gmail.check_inbox(
  path.resolve(__dirname, process.env.GOOGLE_CREDENTIALS), // Assuming credentials.json is in the current directory.
  path.resolve(__dirname, process.env.GOOGLE_TOKEN), // Look for gmail_token.json in the current directory (if it doesn't exists, it will be created by the script).
  {
    //5v1e47primeoplogin+500q@gmail.com: Welcome to Prime Opinion
    subject: emailSubject, // We are looking for 'Activate Your Account' in the subject of the message.
    from: process.env.FROM_EMAIL, // We are looking for a sender header which is 'no-reply@domain.com'.
    to: toEmail, // Which inbox to poll. credentials.json should contain the credentials to it.
    wait_time_sec: 10, // Poll interval (in seconds).
    max_wait_time_sec: 30, // Maximum poll time (in seconds), after which we'll giveup.
    include_body: true
  }
).then(email => {
  return processLoginUrl(email[0].body.html)
})};

 function processLoginUrl(html) {
  const url = extractSignatureLoginUrl(html)

  if (!url) {
    // DO something
    return
  }
  return url;

  // DO something else
}

function extractSignatureLoginUrl(inputText) {
  // Regular expression to match URLs containing "signature-login"
  const urlRegex = /https?:\/\/[^\s]*signature-login[^\s]*/i;

  // Find the first match in the input text
  const match = inputText.match(urlRegex);

  if (match) {
    // Extract and return the matched URL
    return match[0];
  } else {
    // If no match is found, return null
    return null;
  }
}
module.exports = getUrl;