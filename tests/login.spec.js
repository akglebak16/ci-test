import { expect, test } from "@playwright/test";
import { HomePage } from '../pages/HomePage';

test('Login @smoke', async ({ page }) => {
  const Hoдme = new HomePage(page);
  const account = page.getByRole('link', { name: 'Account' });
  await Home.goToHomePage()
  await Home.closeCookiesWindow()
  await Home.login();
  await account.click();
  await expect (page.locator('[class="profile--user-email"]')).toBeVisible()
});

test("Login With Invalid Email", async ({page})=>{
  const Home = new HomePage(page);
  const email = 'email@gfgfsdfdss'

  await Home.goToHomePage()
  await Home.closeCookiesWindow()
  await Home.enteringTheEmail(email);
  await expect (page.locator(".prime-input.error.has-value")).toBeVisible()
});

test("Login With Empty Email", async ({page})=>{
  const Home = new HomePage(page);
  const email = ''
  
  await Home.goToHomePage()
  await Home.closeCookiesWindow()
  await Home.enteringTheEmail(email);
  await expect (page.locator('[class="prime-input error"]')).toBeVisible()
});