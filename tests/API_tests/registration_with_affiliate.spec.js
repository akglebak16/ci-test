import { test, expect } from "@playwright/test";
import { HomePage } from '../../pages/HomePage';
import generateRandomEmail from '../../utils/email_generator';

test("Registration With Affiliate Data", async ({ page }) => {
    const homePage = new HomePage(page);
    const affiliatePartOfUrl = '?txid=abc123&offer_id=1&aff_id=1000&aff_sub=sub12345'
    const secondAffiliatePartOfUrl = '?txid=abc321&offer_id=2&aff_id=1001&aff_sub=sub12341'
    const correctResponse = {
        aff_id: "1000",
        aff_sub: "sub12345",
        offer_id: "1",
        txid: "abc123"
    };

    const correctResponse2 = {
        aff_id: "1001",
        aff_sub: "sub12341",
        offer_id: "2",
        txid: "abc321"
    };

    await page.goto(process.env.HOME_URL + affiliatePartOfUrl);
    await homePage.closeCookiesWindow();

    await homePage.enteringTheEmail("primeoplogin+" + generateRandomEmail() + "gmail.com");
    await page.getByPlaceholder('Password').fill('12131213');
    await page.locator('label.checkbox-label').click();

    const [request] = await Promise.all([
        page.waitForRequest('https://api.primeopinion.com/auth/register'),
        page.click('div button.prime-button.submit-button')        
    ]);

    const postData = request.postData();
    const postDataObject = JSON.parse(postData);
    const filteredData = {
        txid: postDataObject.txid,
        offer_id: postDataObject.offer_id,
        aff_id: postDataObject.aff_id,
        aff_sub: postDataObject.aff_sub
    };

    console.log(filteredData)
    const response = await request.response();
    expect(filteredData).toEqual(correctResponse);

    await page.goto(process.env.HOME_URL + secondAffiliatePartOfUrl);

    await homePage.enteringTheEmail("primeoplogin+" + generateRandomEmail() + "gmail.com");
    await page.getByPlaceholder('Password').fill('12131213');
    await page.locator('label.checkbox-label').click();
    const [SecondRequest] = await Promise.all([
        page.waitForRequest('https://api.primeopinion.com/auth/register'),
        page.click('div button.prime-button.submit-button')        
    ]);

    const postData2 = SecondRequest.postData();
    const postDataObject2 = JSON.parse(postData2);
    const filteredData2 = {
        txid: postDataObject2.txid,
        offer_id: postDataObject2.offer_id,
        aff_id: postDataObject2.aff_id,
        aff_sub: postDataObject2.aff_sub
    };
    console.log(filteredData2)
    const response2 = await SecondRequest.response();
    expect(filteredData2).toEqual(correctResponse2);

});
