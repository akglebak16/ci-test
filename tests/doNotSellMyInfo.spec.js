import { test,expect } from "@playwright/test";
import { HomePage } from '../pages/HomePage';

test("Do Not Sell My Personal Info Page is Available", async ({ page }) => {
    const Home = new HomePage(page);
    const correctText = 'Do Not Sell My Personal Information'
    const testemail = 'test@test.test'
    const emailPlaceholderText = page.locator('[class="page-title mb-4"]');
    await Home.goToDoNotSellMyInfoAndEnterTheEmail(testemail);
    await expect(emailPlaceholderText).toContainText(correctText)
  });