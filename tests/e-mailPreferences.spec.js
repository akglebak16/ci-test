import { test, expect } from "@playwright/test";
import { AccountPage } from '../pages/AccountPage';
import { HomePage } from '../pages/HomePage';

test.fixme("Email Preferences could be changed", async ({ page }) => {
    const AccountP = new AccountPage(page);
    const Home = new HomePage(page);
    const AccountUpdatesSwitcher = page.locator('[data-pc-name="inputswitch"]').nth(0)
    const RemindersSwitcher = page.locator('[data-pc-name="inputswitch"]').nth(2)
    await Home.goToHomePage()
    await Home.CloseCookiesWindow()
    await Home.login();
    await AccountP.GoToSettings();
    await AccountP.goToEmailPreferencesAndSwitch();
    await AccountP.SaveEmailPreferences();
    await expect(AccountUpdatesSwitcher).not.toHaveClass('p-inputswitch p-component p-inputswitch-checked');
    await expect(RemindersSwitcher).not.toHaveClass('p-inputswitch p-component p-inputswitch-checked');
    await AccountP.GoToSettings();
    await AccountP.goToEmailPreferencesAndSwitch();
    await AccountP.SaveEmailPreferences();
    await expect(AccountUpdatesSwitcher).toHaveClass('p-inputswitch p-component p-inputswitch-checked');
    await expect(RemindersSwitcher).toHaveClass('p-inputswitch p-component p-inputswitch-checked');

});