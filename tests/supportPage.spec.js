import { test, expect } from "@playwright/test";
import { MorePage } from '../pages/MorePage';
import { HomePage } from '../pages/HomePage';

test("Support Page Is Available", async ({ page }) => {
    const MoreP = new MorePage(page);
    const Home = new HomePage(page);
    await Home.goToHomePage()
    await Home.closeCookiesWindow()
    await Home.login();
    await MoreP.goToSupportPage('email@gmail.com', 'helpme')
    
});