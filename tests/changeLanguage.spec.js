import { test, expect } from "@playwright/test";
import { AccountPage } from '../pages/AccountPage';
import { HomePage } from '../pages/HomePage';

test("Change Language", async ({ page }) => {
    const AccountP = new AccountPage(page);
    const Home = new HomePage(page);
    await Home.goToHomePage()
    
    await Home.closeCookiesWindow()
    await Home.login(process.env.TEST_USERNAME2,'12131213');
    await AccountP.GoToSettings();
    await AccountP.ChangeLanguageToSpain();
    await page.waitForLoadState();
    await expect(page.getByRole('link', { name: 'Encuestas' })).toContainText('Encuestas');
    await AccountP.ChangeLanguageToEnglish();
    await expect(page.getByRole('link', { name: 'Surveys' })).toBeVisible();
    await page.waitForLoadState();
    
    });