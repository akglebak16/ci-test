import { test,expect } from "@playwright/test";

test("redirect to App store", async ({ page }) => {
    await page.goto(process.env.REDIRECT_TO_APPSTRORE);
    await expect(page).toHaveURL('https://apps.apple.com/us/app/prime-opinion-paid-surveys/id6450182934');
  });

  test("redirect to Google play", async ({ page }) => {
    await page.goto(process.env.REDIRECT_TO_GOOGLEPLAY);
    await expect(page).toHaveURL('https://play.google.com/store/apps/details?id=com.primeopinion.appname');
  });

  test("redirect With Wrong Platform", async ({ page }) => {
    await page.goto(process.env.REDIRECT_WITH_WRONG_PLATFORM);
    await expect(page.locator('.prime-input')).toBeVisible()
  });

  test("redirec tWith No Platform", async ({ page }) => {
    await page.goto(process.env.REDIRECT_NO_PLATFORM);
    await expect(page.locator('.prime-input')).toBeVisible()
  }); 

  test("redirect With Broken Parameter", async ({ page }) => {
    await page.goto(process.env.REDIRECT_WITH_BROKEN_PARAMETER);
    await expect(page.locator('.error-404__description')).toBeVisible()
  }); 