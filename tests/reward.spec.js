import { test, expect } from "@playwright/test";
import { SurveysPage } from "../pages/SurveysPage";
import { HomePage } from '../pages/HomePage';
import generateRandomEmail from '../utils/email_generator';

test.fixme("Select And Claim Reward" , async ({ page }) => {
  const Home = new HomePage(page);
  const Surveys = new SurveysPage(page);
  const FinalClaimButton = page.locator('button.confirm-button')
  await Home.goToHomePage()
  await Home.CloseCookiesWindow()
  await Home.login();
  await Surveys.changeOrSelectReward();
  await Surveys.claimReward();
  await expect(FinalClaimButton).toBeVisible()
  await expect(FinalClaimButton).toContainText('Claim Reward')
});

test.fixme("Select And Claim PayPal", async ({ page }) => {
  const Home = new HomePage(page);
  const Surveys = new SurveysPage(page);
  const FinalClaimButton = page.locator('button.confirm-button')
  const SelectedRewardName = page.locator('div.user-rewards .reward-description span').nth(1)
  await Home.goToHomePage()
  await Home.CloseCookiesWindow()
  await Home.login();
  await Surveys.changeOrSelectReward('1','0');
  await expect(SelectedRewardName).toContainText('PayPal')
  await Surveys.claimReward();
  await expect(FinalClaimButton).toBeVisible()
  await expect(FinalClaimButton).toContainText('Claim Reward')
  await Surveys.enterAndChangePayPalEmail(generateRandomEmail() + "gmail.com")
  await expect(FinalClaimButton).toBeVisible()
});