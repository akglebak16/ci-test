import { test, expect } from "@playwright/test";
import { SurveysPage } from '../pages/SurveysPage';
import { HomePage } from '../pages/HomePage';

test("Welcom Bonus Claiming", async ({ page }) => {
    const SurveysP = new SurveysPage(page);
    const Home = new HomePage(page); 
    const email = await Home.createAccount();

    await Home.confirmEmail(email);
    await SurveysP.claimWelcomeBonus();
    await expect(page.locator('.user-balance-amount')).toContainText('200 points')
    
});