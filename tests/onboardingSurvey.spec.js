import { test } from "@playwright/test";
import { SurveysPage } from "../pages/SurveysPage";
import { HomePage } from '../pages/HomePage';

test("onboarding Survey Can Be Finished", async ({ page }) => {
    const SurveysP = new SurveysPage(page);
    const Home = new HomePage(page); 
    const email = await Home.createAccount();
    await Home.confirmEmail(email);
    await SurveysP.claimWelcomeBonus();
    await SurveysP.passOnboardingSurvey();
})
