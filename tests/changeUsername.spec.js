import { test, expect } from "@playwright/test";
import { AccountPage } from '../pages/AccountPage';
import { HomePage } from '../pages/HomePage';

test("change username", async ({ page }) => {
    const AccountP = new AccountPage(page);
    const Home = new HomePage(page);
    const TestName = 'SecondTestName1337'
    const TestName2 = 'FirstTestName1337'
    await Home.goToHomePage()
    await Home.closeCookiesWindow()
    await Home.login();
    await AccountP.ChangeUsername(TestName);
    await expect(page.locator('[class="profile--user-name"]')).toContainText(TestName);
    await AccountP.ChangeUsername(TestName2);
    await expect(page.locator('[class="profile--user-name"]')).toContainText(TestName2);




    });