import { test, expect } from "@playwright/test";
import { AccountPage } from '../pages/AccountPage';
import { HomePage } from '../pages/HomePage';

test("Change Timezone", async ({ page }) => {
    const AccountP = new AccountPage(page);
    const Home = new HomePage(page);
    await Home.goToHomePage()
    await Home.closeCookiesWindow()
    await Home.login();
    const ChangeTime = await AccountP.ChangeTimeZone();
    await page.waitForLoadState();
    expect(ChangeTime.USTimeInProfile).not.toEqual(ChangeTime.changedTimeInProfile)
});