import { test, expect } from "@playwright/test";
import { HomePage } from '../pages/HomePage';
import randomRestrictedEmail from '../utils/restricted_email_generator';
import generateRandomEmail from '../utils/email_generator';

test("Registration With Valid Credentials", async ({ page }) => {
    const Home = new HomePage(page);
    const welcomeMessageText = "Choose your Welcome Bonus";
    const email = await Home.createAccount();
    const continueRegButton = page.locator('[class="prime-button submit-button p-button p-component button gradient"]');
    
    await Home.confirmEmail(email);
    await expect(page.locator('div.title-wrapper')).toContainText(welcomeMessageText)
  
  });

  test("registrationWithNonSupportedEmail", async ({ page }) => {
    const Home = new HomePage(page);
    const errorText = "We are not able to create you an account with that email.";
    await Home.goToHomePage()
    await Home.closeCookiesWindow()
    await Home.enteringCredentialsForRegistration('userboozer@' + randomRestrictedEmail(), process.env.PASSWORD);
    await expect(page.locator('div.content-form div.prime-input__footer ').nth(0)).toContainText(errorText)
  
  });
  
  test("registrationWithShortPassword", async ({ page }) => {
    const Home = new HomePage(page);
    const errorText = "Password should be at least 8 characters long";
    await Home.createAccount(generateRandomEmail(), '1234567')
    await expect(page.locator('div.content-form div.prime-input__footer').nth(1)).toContainText(errorText)
    
  
  });
