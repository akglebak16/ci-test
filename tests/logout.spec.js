import { test, expect } from "@playwright/test";
import { AccountPage } from '../pages/AccountPage';
import { HomePage } from '../pages/HomePage';

test("logout @smoke", async ({ page }) => {
    const AccountP = new AccountPage(page);
    const Home = new HomePage(page);
    await Home.goToHomePage()
    await Home.closeCookiesWindow()
    await Home.login();
    await AccountP.GoToSettings();
    await AccountP.logOut()
});