import { test, expect } from "@playwright/test";
import { AccountPage } from '../pages/AccountPage';
import { HomePage } from '../pages/HomePage';

test("Change password", async ({ page }) => {
    const AccountP = new AccountPage(page);
    const Home = new HomePage(page);
    await Home.goToHomePage()
    await Home.closeCookiesWindow()
    await Home.login(process.env.TEST_USERNAME3,process.env.PASSWORD);
    await AccountP.GoToSettings();
    await AccountP.changeUserPassword(process.env.PASSWORD,"11111111");
    await AccountP.logOut()
    await Home.goToHomePage()
    await Home.login(process.env.TEST_USERNAME3, "11111111");
    await AccountP.GoToSettings();
    await AccountP.changeUserPassword("11111111",process.env.PASSWORD);
   
});