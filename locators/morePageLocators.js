export class Locators {
    constructor(page){
        this.page = page;
        
        this.morePageTab = page.locator('[class="desktop-nav-link"]').nth(2);
        this.helpButton = page.locator('.prime-navigation-icon').first();
        this.askQuestionButton = page.getByRole('button', { name: 'Ask Question' });
        this.enterEmailField = page.getByPlaceholder('Enter your e-mail');
        this.enterMessageField = page.getByPlaceholder('How can we help?');
        this.sendSuppoerTicketButton = page.getByRole('button', { name: 'Submit' });
    }
}
