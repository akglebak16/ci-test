export class Locators {
  constructor(page) {
  this.page = page;

  //Welcome bonus claim
  this.bonus200Points = page.locator('.reward-option.popular');
  this.getWelcomeBonusButton = page.locator('[class="p-btn-label"]').nth(1);
  this.balance = page.locator('.user-balance-amount');

  //OnboardingSurvey
  this.onboardingSurvey = page.locator('[class="surveys__item"]');
  this.startSurveyButton = page.locator('[type="button"]').nth(1);
  this.nextQuestionArrow = page.locator('.p-btn-icon.p-btn-icon--right');
  this.englishCheckbox = page.locator('label').filter({ hasText: 'English' });
  this.polishCheckbox = page.locator('[id="pl"]');
  this.enterYearField = page.locator('[class="p-input"]');
  this.selectMounthDropdown = page.locator('.selected-item').nth(0);
  this.selectApril = page.getByText('April');
  this.selectDayDropdown = page.locator('.selected-item').nth(1);
  this.selectBirthday = page.locator('span.dropdown__value').nth(13);
  this.selectMaleGender = page.locator('span.p-radio-box').nth(0);
  this.postalCodeField = page.locator('.p-input');
  this.participateButton = page.locator('div.notice-content button.p-btn.p-btn--gradient');
  this.greatMessage = page.locator('h1.notice-title');
  this.participateButton


  //Claim reward
  this.claimButton = page.getByRole('button', { name: 'claim' });
  this.changeRewardButton = page.getByRole('button', { name: 'Change reward' })
  this.selectRewardButton = page.getByRole('button', { name: 'Select reward' });
  this.SelectRewardType = page.locator('button.p-accordion-button')
  this.SelectRewardAmount = page.locator('div.reward-option');
  this.confirmSelectedReward = page.getByRole('button', { name: 'Select', exact: true });
  
  //PayPal
  this.payPalEmailField = page.locator('[type="email"]')
  this.claimRewardButton = page.locator('[type="button"]')
  this.confirmEmailModal = page.locator('[class="modal-container show"]')
  this.ChangeEmailButton = page.locator('[class="modal-container show"] button.p-btn--outline')

}
}
