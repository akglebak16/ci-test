export class Locators {
  constructor(page){
  this.page = page;

  this.cookiesDialog = page.locator('div div #onetrust-close-btn-container');
  this.cookiesRejectButton = page.getByRole('button', { name: 'Close' });

  this.authDialog = page.locator(".p-dialog > .p-dialog-content");

  this.emailInput = this.authDialog.getByPlaceholder("Email");
  this.passwordInput = page.getByPlaceholder("Password");

  this.signInButton = page.getByRole("button", { name: "Log in or Sign up" });
  this.continueButton = this.authDialog.getByRole("button", {
    name: "Continue",
    exact: true,
  });
  this.loginButton = page.getByRole('button', { name: 'Log in', exact: true });

  //do not sell my info
  this.DoNotSellMyInfoLink = page.locator('[href="/en-us/personal-information"]');
  this.doNotSellMyInfoEmail = page.locator('[placeholder="Enter Email"]');
  }
}
