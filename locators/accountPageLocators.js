export class Locators {
  constructor(page){
     
    this.page = page;
    this.account = page.getByRole('link', { name: 'Account' });
    this.AccountSettings = page.locator('[class="prime-navigation-outlined primary"]').nth(0);
    this.changeUsernameButton = page.getByText('Change Username');
    this.UserNameText = page.locator('[class="profile--user-name"]');
    this.SecondTestName = page.getByText('SecondTestName');
    this.userNameField = page.getByPlaceholder('Enter Username');
    this.saveUsernameButton = page.getByRole('button', { name: 'Save' });
    this.backButton = page.getByRole('main').getByRole('button');

    //language switch to Spain
    this.languageButton = page.locator('.item-title').nth(4);
    this.spainCheckbox =  page.locator('span.p-radio-box').nth(1);
    this.saveLanguageButton = page.getByRole('button', { name: 'Save' });
    this.surveysSpainTab =  page.getByRole('link', { name: 'Encuestas' });

    //switch to English
    this.englishCheckbox = page.locator('span.p-radio-box').nth(0);
    this.saveLanguageButtonToEng = page.getByRole('button', { name: 'Guarda' });
    this.languageButtonEng = page.locator('div').filter({ hasText: /^Lengua$/ });
  
    //change timezone
    this.timeInProfile = page.locator('span.time-value');
    this.changeTimeZone = page.getByText('Change Timezone');
    this.timeZoneDropDown = page.locator('div.content-inputs');
    this.Pacific_Time = page.getByRole('option', { name: '-08:00 Pacific Time (US & Canada)' });
    this.saveTimezoneButton = page.getByRole('button', { name: 'Save' });
    this.US_Time = page.getByRole('option', { name: '-05:00 Eastern Time (US & Canada) New York' });

    //change Password locators
    this.changePasswordButton = page.locator('div.settings-item-wrapper:nth-child(6)');
    this.enterOldPasswordField = page.getByPlaceholder('Old Password');
    this.enterNewPasswordField = page.getByPlaceholder('Enter new password');
    this.confirmPasswordField = page.getByPlaceholder('Confirm Password');
    this.confirmPasswordChangingButton = page.locator('div button.p-btn.p-btn--gradient')
    
    //logout
    this.logOutButton = page.locator('section div.prime-navigation-outlined').nth(2);

    //email preferences
    this.emailPreferencesButton = page.locator('div.settings-item-wrapper:nth-child(4)');
    this.AccountUpdatesSlider = page.locator('span.p-inputswitch-slider').nth(0)
    this.RemindersUpdatesSlider = page.locator('span.p-inputswitch-slider').nth(2)
    this.UpdatePreferencesButton = page.locator('div button.p-btn--gradient')
  } 
}
