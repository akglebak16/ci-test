function generateRandomEmail() {
    const domains = ["gmail.com", "yahoo.com", "hotmail.com", "outlook.com", "example.com"];
    
    // Generate a random username
    const usernameLength = Math.floor(Math.random() * 9) + 5; // Random length between 5 and 14 characters
    const username = [...Array(usernameLength)]
      .map(() => (Math.random() > 0.5 ? String.fromCharCode(Math.floor(Math.random() * 26) + 97) : Math.floor(Math.random() * 10)))
      .join("");
    
    // Randomly select a domain
    const domain = domains[Math.floor(Math.random() * domains.length)];
    
    return `${username}@`;
  }
  export default generateRandomEmail