## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Playwright](https://marketplace.visualstudio.com/items?itemName=ms-playwright.playwright) + [GmailTested](https://www.npmjs.com/package/gmail-tester)

### Run tests using Staging env

```sh
npm run test-stage
```

### Run tests using Production env

```sh
npm run test-prod
```

### Run tests in Debug mode using Staging env

```sh
npm run test-debug
```

### Show the last tests' report

```sh
npm run test-report
```

### Run codegen (selector helper) for improve speed in testing development. After that you should paste URL of the App

```sh
npx playwright codegen
```
