import {Locators} from "../locators/morePageLocators"

export class MorePage extends Locators {
    constructor(page) {
        super(page);
        
}       
    async goToSupportPage (email,message){
        await this.morePageTab.click()
        await this.helpButton.click()
        await this.askQuestionButton.click()
        await this.enterEmailField.fill(email)
        await this.enterMessageField.fill(message)
        await this.sendSuppoerTicketButton.click()
}
}
