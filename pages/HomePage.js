import { expect } from "@playwright/test";
import generateRandomEmail from '../utils/email_generator';
import { Locators } from "../locators/homePageLocators";
const emailer = require("../GmailAPI/emails")


export class HomePage extends Locators {
  constructor(page) {
    super(page);
    
  }

  async goToHomePage(){
    await this.page.goto(process.env.HOME_URL);
    await expect(this.page).toHaveTitle(/Online Paid Surveys/);
  }
  async closeCookiesWindow(){
    await this.cookiesDialog.isVisible()
    await this.cookiesRejectButton.click()
  }
  async enteringTheEmail (email = process.env.TEST_USERNAME){
    await this.signInButton.click({ timeout: 10000 });
    await expect(this.emailInput).toBeVisible();
    await this.emailInput.fill(email);
    await this.continueButton.click();

  }

  async enteringThePassword(password = process.env.PASSWORD) {
    await expect(this.passwordInput).toBeVisible();
    await this.passwordInput.fill(password);
    await this.loginButton.click();
    await expect(this.page).toHaveTitle(/Prime Opinion/);
  }
  async login(email= process.env.TEST_USERNAME,password= process.env.PASSWORD) {
    await this.enteringTheEmail(email);
    await this.enteringThePassword(password);
  }

  async createAccount(email = generateRandomEmail(), password = process.env.PASSWORD) {
    await this.page.goto(process.env.HOME_URL);
    await expect(this.page).toHaveTitle(/Online Paid Surveys/);
    await expect(this.cookiesDialog).toBeVisible({ timeout: 10000 });
    await this.cookiesRejectButton.click();
    await this.page.getByPlaceholder('Email').fill("primeoplogin+" + email + "gmail.com");
    await this.page.getByRole('button', { name: 'Continue' }).click();
    await this.page.getByPlaceholder('Password').fill(password);
    await this.page.locator('label').filter({ hasText: 'I accept the Privacy Policy and Terms & Conditions' }).click();
    await this.page.locator('div button.prime-button.submit-button').click();
    return email;
    }
  
  async AcceptPrivacyAndContinue(){
    await this.page.locator('label.checkbox-label').check();
    await this.page.locator('div button.prime-button.submit-button').click();
  }
  async enteringCredentialsForRegistration(email,password){
    await this.enteringTheEmail(email);
    await this.page.getByPlaceholder('Password').fill(password);
    await this.AcceptPrivacyAndContinue();
    }

  async confirmEmail(email) {
    const url = await emailer(email)
    console.log(url)
    await this.page.goto(url)
    
  }

  async goToDoNotSellMyInfoAndEnterTheEmail(email){
    await this.goToHomePage()
    await this.DoNotSellMyInfoLink.click()
    await this.doNotSellMyInfoEmail.fill(email)
  }

}
