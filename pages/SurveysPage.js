import { expect } from "@playwright/test";
import { Locators } from "../locators/surveysPageLocators";
export class SurveysPage extends Locators{
    constructor(page) {
      super(page);
    
  }
  
  async goToSurveysPage() {
    await this.page.goto(`${process.env.BASE_URL}/surveys`);
    await expect(this.page).toHaveTitle(/.*Prime Opinion/);
  }

  async claimReward() {
    await this.claimButton.click();
  }

  async changeOrSelectReward(RewardIndex = '0',RewardAmountIndex = '0' ){
    await this.page.waitForTimeout(1500)
    const ChangeRewardButtonExist = this.page.locator('button.user-rewards-button').nth(0)
    const ChangeRewardButtonText = ChangeRewardButtonExist.textContent()
    if (await ChangeRewardButtonText == 'Change reward')
    {
      await this.changeRewardButton.click();
    }
    else {
      await this.selectRewardButton.click();
    }
    await this.SelectRewardType.nth(RewardIndex).click();
    await this.SelectRewardAmount.nth(RewardAmountIndex).click();
    await this.confirmSelectedReward.click();
  }
  async claimWelcomeBonus() {
      await this.bonus200Points.click();
      await this.getWelcomeBonusButton.click();
  }
  
  async passOnboardingSurvey(){
    await this.onboardingSurvey.click();
    await this.startSurveyButton.click();
    await this.nextQuestionArrow.click();
    await this.englishCheckbox.check();
    await this.nextQuestionArrow.click();
    await this.enterYearField.fill('1998');
    await this.selectMounthDropdown.click();
    await this.selectApril.click();
    await this.selectDayDropdown.click();
    await this.selectBirthday.click();
    await this.nextQuestionArrow.click();
    await this.selectMaleGender.click();
    await this.nextQuestionArrow.click();
    await this.postalCodeField.fill('11111');
    await this.nextQuestionArrow.click();
    await expect(this.greatMessage).toContainText('Great!')
    await this.participateButton.click()
  }
  
  async enterAndChangePayPalEmail(PayPalEmail){
    await this.payPalEmailField.fill(PayPalEmail)
    await this.claimRewardButton.click()
    await expect(this.confirmEmailModal).toBeVisible()
    await expect(this.ChangeEmailButton).toContainText('Change Email')
    await this.ChangeEmailButton.click()
    await expect(this.confirmEmailModal).toBeHidden()
  }
}
