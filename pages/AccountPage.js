import {Locators} from "../locators/accountPageLocators"


export class AccountPage extends Locators {
    constructor(page) {
        super(page);
        
}       
    async GoToSettings(){
        await this.account.click();
        await this.AccountSettings.click();


    }

    async ChangeLanguageToSpain(){
        await this.languageButton.click();
        await this.spainCheckbox.click();
        await this.saveLanguageButton.click();


    }

    async ChangeLanguageToEnglish(){
        await this.languageButtonEng.click();
        await this.englishCheckbox.click();
        await this.saveLanguageButtonToEng.click();
        await new Promise(resolve => setTimeout(resolve, 500));
        
    }
    async changeUserPassword(OldPassword,NewPassword) {
        await this.changePasswordButton.click();
        await this.enterOldPasswordField.fill(OldPassword);
        await this.enterNewPasswordField.fill(NewPassword);
        await this.confirmPasswordField.fill(NewPassword);
        await this.confirmPasswordChangingButton.click();
        await this.page.waitForTimeout(500)

    }

    async ChangeUsername(TestName) {
        await this.account.click();
        await this.UserNameText.textContent();
        await this.AccountSettings.click();
        await this.changeUsernameButton.click();
        await this.userNameField.fill(TestName);
        await this.saveUsernameButton.click();
        await this.backButton.click();

        
    }
    async ChangeTimeZone() {
        await this.account.click();
        const USTimeInProfile = await this.timeInProfile.textContent();
        await this.AccountSettings.click();
        await this.changeTimeZone.click();
        await this.timeZoneDropDown.click();
        await this.Pacific_Time.click();
        await this.saveTimezoneButton.click();
        await this.account.click();
        const changedTimeInProfile = await this.timeInProfile.textContent();
        await this.AccountSettings.click();
        await this.changeTimeZone.click();
        await this.timeZoneDropDown.click();
        await this.US_Time.click();
        await this.saveTimezoneButton.click();
        await this.account.click();
        return {USTimeInProfile,changedTimeInProfile}
    }

    async logOut(){
        await this.account.click();
        await this.logOutButton.click();
    }
    async SaveEmailPreferences() {
        await this.UpdatePreferencesButton.click()
    }

    async goToEmailPreferencesAndSwitch(){
        await this.emailPreferencesButton.click()
        await this.AccountUpdatesSlider.click()
        await this.RemindersUpdatesSlider.click()
    }
    
}
